const problem5 = require('./test/testProblem4')

const sol5 = () => {
    let olderCars = problem5.filter((item) => {
        return item > 2000
    })
    return olderCars
}

module.exports = sol5