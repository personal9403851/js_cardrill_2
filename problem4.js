const problem4 = (inventory) => {
    let carYears = inventory.map((item) => {
        return item.car_year
    })
    
    return carYears
}

module.exports = problem4