const problem6 = (inventory) => {
    let BMW_Audi = inventory.filter((item) => {
        return item.car_make === 'BMW' || item.car_make === 'Audi'
    })
    return BMW_Audi
}

module.exports = problem6