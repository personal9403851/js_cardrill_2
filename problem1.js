const problem1 = (inventory) => {
    return inventory.find((item) => {
        return item.id === 33;
    })
};

module.exports = problem1
